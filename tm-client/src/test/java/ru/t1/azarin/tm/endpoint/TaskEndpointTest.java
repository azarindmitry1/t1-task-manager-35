package ru.t1.azarin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.azarin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.azarin.tm.api.endpoint.IProjectEndpoint;
import ru.t1.azarin.tm.api.endpoint.ITaskEndpoint;
import ru.t1.azarin.tm.api.endpoint.IUserEndpoint;
import ru.t1.azarin.tm.api.service.IPropertyService;
import ru.t1.azarin.tm.dto.request.project.ProjectClearRequest;
import ru.t1.azarin.tm.dto.request.project.ProjectCreateRequest;
import ru.t1.azarin.tm.dto.request.task.*;
import ru.t1.azarin.tm.dto.request.user.UserLoginRequest;
import ru.t1.azarin.tm.dto.request.user.UserLogoutRequest;
import ru.t1.azarin.tm.dto.response.project.ProjectCreateResponse;
import ru.t1.azarin.tm.dto.response.task.*;
import ru.t1.azarin.tm.enumerated.Status;
import ru.t1.azarin.tm.marker.IntegrationCategory;
import ru.t1.azarin.tm.model.Project;
import ru.t1.azarin.tm.model.Task;
import ru.t1.azarin.tm.service.PropertyService;

@Category(IntegrationCategory.class)
public final class TaskEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(propertyService);

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService);

    @NotNull
    private final String EMPTY_STRING = "";

    @Nullable
    private final String NULL_STRING = null;

    @NotNull
    private final String TEST_STRING = "TEST_STRING";

    @NotNull
    private final String TEST_STRING2 = "TEST_STRING2";

    @NotNull
    private final String TEST_STRING3 = "TEST_STRING3";

    @Nullable
    private String ADMIN_TOKEN;

    @Nullable
    private Project ADMIN_PROJECT1;

    @Nullable
    private Task ADMIN_TASK1;

    @Nullable
    private Task ADMIN_TASK2;

    @Before
    public void before() {
        ADMIN_TOKEN = authEndpoint.login(new UserLoginRequest("admin", "admin")).getToken();

        @NotNull final ProjectCreateRequest createRequest = new ProjectCreateRequest(ADMIN_TOKEN);
        createRequest.setName(TEST_STRING);
        createRequest.setDescription(TEST_STRING);
        @NotNull final ProjectCreateResponse createResponse = projectEndpoint.createResponse(createRequest);
        ADMIN_PROJECT1 = createResponse.getProject();

        @NotNull final TaskCreateRequest createRequest1 = new TaskCreateRequest(ADMIN_TOKEN);
        createRequest1.setName(TEST_STRING);
        createRequest1.setDescription(TEST_STRING);
        @NotNull final TaskCreateResponse createResponse1 = taskEndpoint.createTaskResponse(createRequest1);
        ADMIN_TASK1 = createResponse1.getTask();

        @NotNull final TaskCreateRequest createRequest2 = new TaskCreateRequest(ADMIN_TOKEN);
        createRequest2.setName(TEST_STRING2);
        createRequest2.setDescription(TEST_STRING2);
        @NotNull final TaskCreateResponse createResponse2 = taskEndpoint.createTaskResponse(createRequest2);
        ADMIN_TASK2 = createResponse2.getTask();
    }

    @After
    public void after() {
        @NotNull final ProjectClearRequest clearRequest = new ProjectClearRequest(ADMIN_TOKEN);
        projectEndpoint.clearResponse(clearRequest);

        @NotNull final TaskClearRequest clearRequest1 = new TaskClearRequest(ADMIN_TOKEN);
        taskEndpoint.clearTaskResponse(clearRequest1);

        @NotNull final UserLogoutRequest request = new UserLogoutRequest(ADMIN_TOKEN);
        authEndpoint.logout(request);
    }

    @Test
    public void changeTaskStatusByIdResponse() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.changeTaskStatusByIdResponse(new TaskChangeStatusByIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.changeTaskStatusByIdResponse(new TaskChangeStatusByIdRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.changeTaskStatusByIdResponse(new TaskChangeStatusByIdRequest(NULL_STRING))
        );
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(ADMIN_TOKEN);
        request.setId(ADMIN_TASK1.getId());
        request.setStatus(Status.IN_PROGRESS);
        @Nullable final TaskChangeStatusByIdResponse response = taskEndpoint.changeTaskStatusByIdResponse(request);
        Assert.assertEquals(Status.IN_PROGRESS, response.getTask().getStatus());
    }

    @Test
    public void changeTaskStatusByIndexResponse() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.changeTaskStatusByIndexResponse(new TaskChangeStatusByIndexRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.changeTaskStatusByIndexResponse(new TaskChangeStatusByIndexRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.changeTaskStatusByIndexResponse(new TaskChangeStatusByIndexRequest(NULL_STRING))
        );
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(ADMIN_TOKEN);
        request.setIndex(0);
        request.setStatus(Status.IN_PROGRESS);
        @Nullable final TaskChangeStatusByIndexResponse response = taskEndpoint.changeTaskStatusByIndexResponse(request);
        Assert.assertEquals(Status.IN_PROGRESS, response.getTask().getStatus());
    }

    @Test
    public void clearTask() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.clearTaskResponse(new TaskClearRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.clearTaskResponse(new TaskClearRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.clearTaskResponse(new TaskClearRequest(NULL_STRING))
        );
        @NotNull final TaskClearRequest request = new TaskClearRequest(ADMIN_TOKEN);
        @Nullable final TaskClearResponse response = taskEndpoint.clearTaskResponse(request);
        Assert.assertNull(taskEndpoint.listTaskResponse(new TaskListRequest(ADMIN_TOKEN)).getTasks());
    }

    @Test
    public void completeTaskById() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.completeTaskByIdResponse(new TaskCompleteByIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.completeTaskByIdResponse(new TaskCompleteByIdRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.completeTaskByIdResponse(new TaskCompleteByIdRequest(NULL_STRING))
        );
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(ADMIN_TOKEN);
        request.setId(ADMIN_TASK1.getId());
        @Nullable final TaskCompleteByIdResponse response = taskEndpoint.completeTaskByIdResponse(request);
        Assert.assertEquals(Status.COMPLETED, response.getTask().getStatus());
    }

    @Test
    public void completeTaskByIndex() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.completeTaskByIndexResponse(new TaskCompleteByIndexRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.completeTaskByIndexResponse(new TaskCompleteByIndexRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.completeTaskByIndexResponse(new TaskCompleteByIndexRequest(NULL_STRING))
        );
        @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest(ADMIN_TOKEN);
        request.setIndex(0);
        @Nullable final TaskCompleteByIndexResponse response = taskEndpoint.completeTaskByIndexResponse(request);
        Assert.assertEquals(Status.COMPLETED, response.getTask().getStatus());
    }

    @Test
    public void createTask() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.createTaskResponse(new TaskCreateRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.createTaskResponse(new TaskCreateRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.createTaskResponse(new TaskCreateRequest(NULL_STRING))
        );
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(ADMIN_TOKEN);
        request.setName(TEST_STRING3);
        request.setDescription(TEST_STRING3);
        @Nullable final TaskCreateResponse response = taskEndpoint.createTaskResponse(request);
        Assert.assertEquals(TEST_STRING3, response.getTask().getName());
        Assert.assertEquals(TEST_STRING3, response.getTask().getDescription());
    }

    @Test
    public void listTaskByProjectId() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.listTaskByProjectIdResponse(new TaskListByProjectIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.listTaskByProjectIdResponse(new TaskListByProjectIdRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.listTaskByProjectIdResponse(new TaskListByProjectIdRequest(NULL_STRING))
        );
        @NotNull final TaskBindToProjectRequest bindRequest = new TaskBindToProjectRequest(ADMIN_TOKEN);
        bindRequest.setProjectId(ADMIN_PROJECT1.getId());
        bindRequest.setTaskId(ADMIN_TASK1.getId());
        taskEndpoint.bindTaskToProjectResponse(bindRequest);
        @NotNull final TaskListByProjectIdRequest listRequest = new TaskListByProjectIdRequest(ADMIN_TOKEN);
        listRequest.setId(ADMIN_PROJECT1.getId());
        @Nullable final TaskListByProjectIdResponse response = taskEndpoint.listTaskByProjectIdResponse(listRequest);
        Assert.assertEquals(1, response.getTasks().size());
    }

    @Test
    public void list() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.listTaskResponse(new TaskListRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.listTaskResponse(new TaskListRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.listTaskResponse(new TaskListRequest(NULL_STRING))
        );
        @NotNull final TaskListRequest request = new TaskListRequest(ADMIN_TOKEN);
        @Nullable final TaskListResponse response = taskEndpoint.listTaskResponse(request);
        Assert.assertEquals(2, response.getTasks().size());
    }

    @Test
    public void removeTaskById() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.removeTaskByIdResponse(new TaskRemoveByIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.removeTaskByIdResponse(new TaskRemoveByIdRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.removeTaskByIdResponse(new TaskRemoveByIdRequest(NULL_STRING))
        );
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(ADMIN_TOKEN);
        request.setId(ADMIN_TASK1.getId());
        @Nullable final TaskRemoveByIdResponse response = taskEndpoint.removeTaskByIdResponse(request);
        Assert.assertEquals(ADMIN_TASK1.getId(), response.getTask().getId());
    }

    @Test
    public void removeTaskByIndex() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.removeTaskByIndexResponse(new TaskRemoveByIndexRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.removeTaskByIndexResponse(new TaskRemoveByIndexRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.removeTaskByIndexResponse(new TaskRemoveByIndexRequest(NULL_STRING))
        );
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(ADMIN_TOKEN);
        request.setIndex(0);
        @Nullable final TaskRemoveByIndexResponse response = taskEndpoint.removeTaskByIndexResponse(request);
        Assert.assertEquals(ADMIN_TASK1.getId(), response.getTask().getId());
    }

    @Test
    public void showTaskById() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.showTaskByIdResponse(new TaskShowByIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.showTaskByIdResponse(new TaskShowByIdRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.showTaskByIdResponse(new TaskShowByIdRequest(NULL_STRING))
        );
        @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest(ADMIN_TOKEN);
        request.setId(ADMIN_TASK1.getId());
        @Nullable final TaskShowByIdResponse response = taskEndpoint.showTaskByIdResponse(request);
        Assert.assertEquals(ADMIN_TASK1.getName(), response.getTask().getName());
        Assert.assertEquals(ADMIN_TASK1.getDescription(), response.getTask().getDescription());
    }

    @Test
    public void showTaskByIndex() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.showTaskByIndexResponse(new TaskShowByIndexRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.showTaskByIndexResponse(new TaskShowByIndexRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.showTaskByIndexResponse(new TaskShowByIndexRequest(NULL_STRING))
        );
        @NotNull final TaskShowByIndexRequest request = new TaskShowByIndexRequest(ADMIN_TOKEN);
        request.setIndex(0);
        @Nullable final TaskShowByIndexResponse response = taskEndpoint.showTaskByIndexResponse(request);
        Assert.assertEquals(ADMIN_TASK1.getName(), response.getTask().getName());
        Assert.assertEquals(ADMIN_TASK1.getDescription(), response.getTask().getDescription());
    }

    @Test
    public void startTaskById() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.startTaskByIdResponse(new TaskStartByIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.startTaskByIdResponse(new TaskStartByIdRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.startTaskByIdResponse(new TaskStartByIdRequest(NULL_STRING))
        );
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(ADMIN_TOKEN);
        request.setId(ADMIN_TASK1.getId());
        @Nullable final TaskStartByIdResponse response = taskEndpoint.startTaskByIdResponse(request);
        Assert.assertEquals(Status.IN_PROGRESS, response.getTask().getStatus());
    }

    @Test
    public void startByIndex() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.startTaskByIndexResponse(new TaskStartByIndexRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.startTaskByIndexResponse(new TaskStartByIndexRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.startTaskByIndexResponse(new TaskStartByIndexRequest(NULL_STRING))
        );
        @NotNull final TaskStartByIndexRequest request = new TaskStartByIndexRequest(ADMIN_TOKEN);
        request.setIndex(0);
        @Nullable final TaskStartByIndexResponse response = taskEndpoint.startTaskByIndexResponse(request);
        Assert.assertEquals(Status.IN_PROGRESS, response.getTask().getStatus());
    }

    @Test
    public void bindTaskToProject() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.bindTaskToProjectResponse(new TaskBindToProjectRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.bindTaskToProjectResponse(new TaskBindToProjectRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.bindTaskToProjectResponse(new TaskBindToProjectRequest(NULL_STRING))
        );
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(ADMIN_TOKEN);
        request.setProjectId(ADMIN_PROJECT1.getId());
        request.setTaskId(ADMIN_TASK1.getId());
        @Nullable final TaskBindToProjectResponse response = taskEndpoint.bindTaskToProjectResponse(request);
        Assert.assertNotNull(response);
    }

    @Test
    public void unbindTaskToProject() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.unbindTaskFromProjectResponse(new TaskUnbindFromProjectRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.unbindTaskFromProjectResponse(new TaskUnbindFromProjectRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.unbindTaskFromProjectResponse(new TaskUnbindFromProjectRequest(NULL_STRING))
        );
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(ADMIN_TOKEN);
        request.setProjectId(ADMIN_PROJECT1.getId());
        request.setTaskId(ADMIN_TASK1.getId());
        @Nullable final TaskUnbindFromProjectResponse response = taskEndpoint.unbindTaskFromProjectResponse(request);
        Assert.assertEquals(null, ADMIN_TASK1.getProjectId());
    }

    @Test
    public void updateTaskById() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.updateTaskByIdResponse(new TaskUpdateByIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.updateTaskByIdResponse(new TaskUpdateByIdRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.updateTaskByIdResponse(new TaskUpdateByIdRequest(NULL_STRING))
        );
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(ADMIN_TOKEN);
        request.setId(ADMIN_TASK1.getId());
        request.setName(TEST_STRING3);
        request.setDescription(TEST_STRING3);
        @Nullable final TaskUpdateByIdResponse response = taskEndpoint.updateTaskByIdResponse(request);
        Assert.assertEquals(TEST_STRING3, response.getTask().getName());
        Assert.assertEquals(TEST_STRING3, response.getTask().getDescription());
    }

    @Test
    public void updateTaskByIndex() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.updateTaskByIndexResponse(new TaskUpdateByIndexRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.updateTaskByIndexResponse(new TaskUpdateByIndexRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.updateTaskByIndexResponse(new TaskUpdateByIndexRequest(NULL_STRING))
        );
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(ADMIN_TOKEN);
        request.setIndex(0);
        request.setName(TEST_STRING3);
        request.setDescription(TEST_STRING3);
        @Nullable final TaskUpdateByIndexResponse response = taskEndpoint.updateTaskByIndexResponse(request);
        Assert.assertEquals(TEST_STRING3, response.getTask().getName());
        Assert.assertEquals(TEST_STRING3, response.getTask().getDescription());
    }

}
