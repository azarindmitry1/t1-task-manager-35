package ru.t1.azarin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.azarin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.azarin.tm.api.service.IPropertyService;
import ru.t1.azarin.tm.dto.request.user.UserLoginRequest;
import ru.t1.azarin.tm.dto.request.user.UserLogoutRequest;
import ru.t1.azarin.tm.dto.request.user.UserViewProfileRequest;
import ru.t1.azarin.tm.dto.response.user.UserLoginResponse;
import ru.t1.azarin.tm.dto.response.user.UserLogoutResponse;
import ru.t1.azarin.tm.dto.response.user.UserViewProfileResponse;
import ru.t1.azarin.tm.marker.IntegrationCategory;
import ru.t1.azarin.tm.service.PropertyService;

@Category(IntegrationCategory.class)
public final class AuthEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private final String ADMIN_LOGIN = "admin";

    @NotNull
    private final String ADMIN_PASSWORD = "admin";

    @NotNull
    private final String EMPTY_STRING = "";

    @Nullable
    private final String NULL_STRING = null;

    @NotNull
    private final String TEST_STRING = "TEST_STRING";

    @Test
    public void login() {
        Assert.assertThrows(Exception.class, () ->
                authEndpoint.login(new UserLoginRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                authEndpoint.login(new UserLoginRequest(EMPTY_STRING, EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                authEndpoint.login(new UserLoginRequest(NULL_STRING, NULL_STRING))
        );
        @NotNull final UserLoginRequest request = new UserLoginRequest(ADMIN_LOGIN, ADMIN_PASSWORD);
        @Nullable final UserLoginResponse response = authEndpoint.login(request);
        Assert.assertNotNull(response.getToken());
        Assert.assertTrue(response.getSuccess());
    }

    @Test
    public void logout() {
        Assert.assertThrows(Exception.class, () ->
                authEndpoint.logout(new UserLogoutRequest())
        );
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest(ADMIN_LOGIN, ADMIN_PASSWORD);
        @Nullable final UserLoginResponse loginResponse = authEndpoint.login(loginRequest);
        @NotNull final UserLogoutRequest logoutRequest = new UserLogoutRequest(loginResponse.getToken());
        @Nullable final UserLogoutResponse logoutResponse = authEndpoint.logout(logoutRequest);
        Assert.assertNotNull(logoutResponse);
        Assert.assertThrows(Exception.class, () ->
                authEndpoint.logout(new UserLogoutRequest(TEST_STRING))
        );
    }

    @Test
    public void viewProfile() {
        Assert.assertThrows(Exception.class, () ->
                authEndpoint.viewProfile(new UserViewProfileRequest())
        );
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest(ADMIN_LOGIN, ADMIN_PASSWORD);
        @Nullable final UserLoginResponse loginResponse = authEndpoint.login(loginRequest);
        @NotNull final UserViewProfileRequest viewProfileRequest = new UserViewProfileRequest(loginResponse.getToken());
        @Nullable final UserViewProfileResponse viewProfileResponse = authEndpoint.viewProfile(viewProfileRequest);
        Assert.assertEquals(ADMIN_LOGIN, viewProfileResponse.getUser().getLogin());
    }

}
