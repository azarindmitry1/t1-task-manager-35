package ru.t1.azarin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.azarin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.azarin.tm.api.endpoint.IUserEndpoint;
import ru.t1.azarin.tm.api.service.IPropertyService;
import ru.t1.azarin.tm.dto.request.user.*;
import ru.t1.azarin.tm.dto.response.user.UserRegistryResponse;
import ru.t1.azarin.tm.dto.response.user.UserUpdateProfileResponse;
import ru.t1.azarin.tm.marker.IntegrationCategory;
import ru.t1.azarin.tm.service.PropertyService;

@Category(IntegrationCategory.class)
public final class UserEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService);

    @NotNull
    private final String EMPTY_STRING = "";

    @Nullable
    private final String NULL_STRING = null;

    @NotNull
    private final String TEST_STRING = "TEST_STRING";

    @Nullable
    private String ADMIN_TOKEN;

    @Before
    public void before() {
        ADMIN_TOKEN = authEndpoint.login(new UserLoginRequest("admin", "admin")).getToken();
    }

    @After
    public void after() {
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(ADMIN_TOKEN);
        authEndpoint.logout(request);
    }

    @Test
    public void changePassword() {
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.changePasswordResponse(new UserChangePasswordRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.changePasswordResponse(new UserChangePasswordRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.changePasswordResponse(new UserChangePasswordRequest(NULL_STRING))
        );
        @NotNull final UserChangePasswordRequest changePasswordRequest = new UserChangePasswordRequest(ADMIN_TOKEN);
        changePasswordRequest.setNewPassword(TEST_STRING);
        userEndpoint.changePasswordResponse(changePasswordRequest);
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(ADMIN_TOKEN);
        authEndpoint.logout(request);
        Assert.assertNotNull(authEndpoint.login(new UserLoginRequest("admin", TEST_STRING)).getToken());
        @NotNull final UserChangePasswordRequest changePasswordRequest1 = new UserChangePasswordRequest(ADMIN_TOKEN);
        changePasswordRequest1.setNewPassword("admin");
        userEndpoint.changePasswordResponse(changePasswordRequest1);
    }

    @Test
    public void lockUser() {
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.lockResponse(new UserLockRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.lockResponse(new UserLockRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.lockResponse(new UserLockRequest(NULL_STRING))
        );
        @NotNull final UserLockRequest lockRequest = new UserLockRequest(ADMIN_TOKEN);
        lockRequest.setLogin("test_user");
        userEndpoint.lockResponse(lockRequest);
        @NotNull final UserLogoutRequest logoutRequest = new UserLogoutRequest(ADMIN_TOKEN);
        authEndpoint.logout(logoutRequest);
        Assert.assertThrows(Exception.class, () ->
                authEndpoint.login(new UserLoginRequest(authEndpoint.login(new UserLoginRequest("test_user", "test_user")).getToken()))
        );
    }

    @Test
    public void unlockUser() {
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.unlockResponse(new UserUnlockRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.unlockResponse(new UserUnlockRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.unlockResponse(new UserUnlockRequest(NULL_STRING))
        );
        @NotNull final UserUnlockRequest unlockRequest = new UserUnlockRequest(ADMIN_TOKEN);
        unlockRequest.setLogin("test_user");
        userEndpoint.unlockResponse(unlockRequest);
        @NotNull final UserLogoutRequest logoutRequest = new UserLogoutRequest(ADMIN_TOKEN);
        authEndpoint.logout(logoutRequest);
        Assert.assertNotNull(authEndpoint.login(new UserLoginRequest("test_user", "test_user")));
    }

    @Test
    public void registry() {
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.registryResponse(new UserRegistryRequest(EMPTY_STRING, TEST_STRING, TEST_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.registryResponse(new UserRegistryRequest(TEST_STRING, EMPTY_STRING, TEST_STRING))
        );
        @NotNull final UserRegistryRequest request = new UserRegistryRequest();
        request.setLogin(TEST_STRING);
        request.setPassword(TEST_STRING);
        request.setEmail(TEST_STRING);
        @Nullable final UserRegistryResponse response = userEndpoint.registryResponse(request);
        Assert.assertNotNull(response.getUser());
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(ADMIN_TOKEN);
        removeRequest.setLogin(TEST_STRING);
        userEndpoint.removeResponse(removeRequest);
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest(TEST_STRING, TEST_STRING)));
    }

    @Test
    public void remove() {
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.removeResponse(new UserRemoveRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.removeResponse(new UserRemoveRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.removeResponse(new UserRemoveRequest(NULL_STRING))
        );
        @NotNull final UserRegistryRequest registryRequest = new UserRegistryRequest();
        registryRequest.setLogin(TEST_STRING);
        registryRequest.setPassword(TEST_STRING);
        registryRequest.setEmail(TEST_STRING);
        userEndpoint.registryResponse(registryRequest);
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(ADMIN_TOKEN);
        removeRequest.setLogin(TEST_STRING);
        userEndpoint.removeResponse(removeRequest);
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest(TEST_STRING, TEST_STRING)));
    }

    @Test
    public void updateProfile() {
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.updateProfileResponse(new UserUpdateProfileRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.updateProfileResponse(new UserUpdateProfileRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.updateProfileResponse(new UserUpdateProfileRequest(NULL_STRING))
        );
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(ADMIN_TOKEN);
        request.setFirstName(TEST_STRING);
        request.setMiddleName(TEST_STRING);
        request.setLastName(TEST_STRING);
        @Nullable final UserUpdateProfileResponse response = userEndpoint.updateProfileResponse(request);
        Assert.assertEquals(TEST_STRING, response.getUser().getFirstName());
        Assert.assertEquals(TEST_STRING, response.getUser().getMiddleName());
        Assert.assertEquals(TEST_STRING, response.getUser().getLastName());
    }

}
