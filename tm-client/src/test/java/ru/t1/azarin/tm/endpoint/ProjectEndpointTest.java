package ru.t1.azarin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.azarin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.azarin.tm.api.endpoint.IProjectEndpoint;
import ru.t1.azarin.tm.api.endpoint.IUserEndpoint;
import ru.t1.azarin.tm.api.service.IPropertyService;
import ru.t1.azarin.tm.dto.request.project.*;
import ru.t1.azarin.tm.dto.request.user.UserLoginRequest;
import ru.t1.azarin.tm.dto.request.user.UserLogoutRequest;
import ru.t1.azarin.tm.dto.response.project.*;
import ru.t1.azarin.tm.enumerated.Status;
import ru.t1.azarin.tm.marker.IntegrationCategory;
import ru.t1.azarin.tm.model.Project;
import ru.t1.azarin.tm.service.PropertyService;

@Category(IntegrationCategory.class)
public final class ProjectEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService);

    @NotNull
    private final String EMPTY_STRING = "";

    @Nullable
    private final String NULL_STRING = null;

    @NotNull
    private final String TEST_STRING = "TEST_STRING";

    @NotNull
    private final String TEST_STRING2 = "TEST_STRING2";

    @NotNull
    private final String TEST_STRING3 = "TEST_STRING3";

    @Nullable
    private String ADMIN_TOKEN;

    @Nullable
    private Project ADMIN_PROJECT1;

    @Nullable
    private Project ADMIN_PROJECT2;

    @Before
    public void before() {
        ADMIN_TOKEN = authEndpoint.login(new UserLoginRequest("admin", "admin")).getToken();

        @NotNull final ProjectCreateRequest createRequest1 = new ProjectCreateRequest(ADMIN_TOKEN);
        createRequest1.setName(TEST_STRING);
        createRequest1.setDescription(TEST_STRING);
        @NotNull final ProjectCreateResponse createResponse1 = projectEndpoint.createResponse(createRequest1);
        ADMIN_PROJECT1 = createResponse1.getProject();

        @NotNull final ProjectCreateRequest createRequest2 = new ProjectCreateRequest(ADMIN_TOKEN);
        createRequest2.setName(TEST_STRING2);
        createRequest2.setDescription(TEST_STRING2);
        @NotNull final ProjectCreateResponse createResponse2 = projectEndpoint.createResponse(createRequest2);
        ADMIN_PROJECT2 = createResponse2.getProject();
    }

    @After
    public void after() {
        @NotNull final ProjectClearRequest clearRequest = new ProjectClearRequest(ADMIN_TOKEN);
        projectEndpoint.clearResponse(clearRequest);

        @NotNull final UserLogoutRequest request = new UserLogoutRequest(ADMIN_TOKEN);
        authEndpoint.logout(request);
    }

    @Test
    public void changeStatusById() {
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.changeStatusByIdResponse(new ProjectChangeStatusByIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.changeStatusByIdResponse(new ProjectChangeStatusByIdRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.changeStatusByIdResponse(new ProjectChangeStatusByIdRequest(NULL_STRING))
        );
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(ADMIN_TOKEN);
        request.setId(ADMIN_PROJECT1.getId());
        request.setStatus(Status.IN_PROGRESS);
        @Nullable final ProjectChangeStatusByIdResponse response = projectEndpoint.changeStatusByIdResponse(request);
        Assert.assertEquals(Status.IN_PROGRESS, response.getProject().getStatus());
    }

    @Test
    public void changeStatusByIndex() {
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.changeStatusByIndexResponse(new ProjectChangeStatusByIndexRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.changeStatusByIndexResponse(new ProjectChangeStatusByIndexRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.changeStatusByIndexResponse(new ProjectChangeStatusByIndexRequest(NULL_STRING))
        );
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(ADMIN_TOKEN);
        request.setIndex(0);
        request.setStatus(Status.IN_PROGRESS);
        @Nullable final ProjectChangeStatusByIndexResponse response = projectEndpoint.changeStatusByIndexResponse(request);
        Assert.assertEquals(Status.IN_PROGRESS, response.getProject().getStatus());
    }

    @Test
    public void clear() {
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.clearResponse(new ProjectClearRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.clearResponse(new ProjectClearRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.clearResponse(new ProjectClearRequest(NULL_STRING))
        );
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(ADMIN_TOKEN);
        @Nullable final ProjectClearResponse response = projectEndpoint.clearResponse(request);
        Assert.assertNull(projectEndpoint.listResponse(new ProjectListRequest(ADMIN_TOKEN)).getProjects());
    }

    @Test
    public void completeById() {
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.completeByIdResponse(new ProjectCompleteByIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.completeByIdResponse(new ProjectCompleteByIdRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.completeByIdResponse(new ProjectCompleteByIdRequest(NULL_STRING))
        );

        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(ADMIN_TOKEN);
        request.setId(ADMIN_PROJECT1.getId());
        @Nullable final ProjectCompleteByIdResponse response = projectEndpoint.completeByIdResponse(request);
        Assert.assertEquals(Status.COMPLETED, response.getProject().getStatus());
    }

    @Test
    public void completeByIndex() {
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.completeByIndexResponse(new ProjectCompleteByIndexRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.completeByIndexResponse(new ProjectCompleteByIndexRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.completeByIndexResponse(new ProjectCompleteByIndexRequest(NULL_STRING))
        );
        @NotNull final ProjectCompleteByIndexRequest request = new ProjectCompleteByIndexRequest(ADMIN_TOKEN);
        request.setIndex(0);
        @Nullable final ProjectCompleteByIndexResponse response = projectEndpoint.completeByIndexResponse(request);
        Assert.assertEquals(Status.COMPLETED, response.getProject().getStatus());
    }

    @Test
    public void create() {
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.createResponse(new ProjectCreateRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.createResponse(new ProjectCreateRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.createResponse(new ProjectCreateRequest(NULL_STRING))
        );
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(ADMIN_TOKEN);
        request.setName(TEST_STRING3);
        request.setDescription(TEST_STRING3);
        @Nullable final ProjectCreateResponse response = projectEndpoint.createResponse(request);
        Assert.assertEquals(TEST_STRING3, response.getProject().getName());
        Assert.assertEquals(TEST_STRING3, response.getProject().getDescription());
    }

    @Test
    public void list() {
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.listResponse(new ProjectListRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.listResponse(new ProjectListRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.listResponse(new ProjectListRequest(NULL_STRING))
        );
        @Nullable final ProjectListResponse response = projectEndpoint.listResponse(new ProjectListRequest(ADMIN_TOKEN));
        Assert.assertNotNull(response.getProjects());
        Assert.assertEquals(2, response.getProjects().size());
    }

    @Test
    public void removeById() {
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.removeByIdResponse(new ProjectRemoveByIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.removeByIdResponse(new ProjectRemoveByIdRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.removeByIdResponse(new ProjectRemoveByIdRequest(NULL_STRING))
        );
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(ADMIN_TOKEN);
        request.setId(ADMIN_PROJECT1.getId());
        @Nullable final ProjectRemoveByIdResponse response = projectEndpoint.removeByIdResponse(request);
        Assert.assertEquals(ADMIN_PROJECT1.getId(), response.getProject().getId());
    }

    @Test
    public void removeByIndex() {
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.removeByIndexResponse(new ProjectRemoveByIndexRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.removeByIndexResponse(new ProjectRemoveByIndexRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.removeByIndexResponse(new ProjectRemoveByIndexRequest(NULL_STRING))
        );
        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest(ADMIN_TOKEN);
        request.setIndex(0);
        @Nullable final ProjectRemoveByIndexResponse response = projectEndpoint.removeByIndexResponse(request);
        Assert.assertEquals(1, projectEndpoint.listResponse(new ProjectListRequest(ADMIN_TOKEN)).getProjects().size());
    }

    @Test
    public void startById() {
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.startByIdResponse(new ProjectStartByIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.startByIdResponse(new ProjectStartByIdRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.startByIdResponse(new ProjectStartByIdRequest(NULL_STRING))
        );
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest(ADMIN_TOKEN);
        request.setId(ADMIN_PROJECT1.getId());
        @Nullable final ProjectStartByIdResponse response = projectEndpoint.startByIdResponse(request);
        Assert.assertEquals(Status.IN_PROGRESS, response.getProject().getStatus());
    }

    @Test
    public void startByIndex() {
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.startByIndexResponse(new ProjectStartByIndexRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.startByIndexResponse(new ProjectStartByIndexRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.startByIndexResponse(new ProjectStartByIndexRequest(NULL_STRING))
        );
        @NotNull final ProjectStartByIndexRequest request = new ProjectStartByIndexRequest(ADMIN_TOKEN);
        request.setIndex(0);
        @Nullable final ProjectStartByIndexResponse response = projectEndpoint.startByIndexResponse(request);
        Assert.assertEquals(Status.IN_PROGRESS, response.getProject().getStatus());
    }

    @Test
    public void updateById() {
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.updateByIdResponse(new ProjectUpdateByIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.updateByIdResponse(new ProjectUpdateByIdRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.updateByIdResponse(new ProjectUpdateByIdRequest(NULL_STRING))
        );
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(ADMIN_TOKEN);
        request.setId(ADMIN_PROJECT1.getId());
        request.setName(TEST_STRING3);
        request.setDescription(TEST_STRING3);
        @Nullable final ProjectUpdateByIdResponse response = projectEndpoint.updateByIdResponse(request);
        Assert.assertEquals(TEST_STRING3, response.getProject().getName());
        Assert.assertEquals(TEST_STRING3, response.getProject().getDescription());
    }

    @Test
    public void updateByIndex() {
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.updateByIndexResponse(new ProjectUpdateByIndexRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.updateByIndexResponse(new ProjectUpdateByIndexRequest(EMPTY_STRING))
        );
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.updateByIndexResponse(new ProjectUpdateByIndexRequest(NULL_STRING))
        );
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(ADMIN_TOKEN);
        request.setIndex(0);
        request.setName(TEST_STRING3);
        request.setDescription(TEST_STRING3);
        @Nullable final ProjectUpdateByIndexResponse response = projectEndpoint.updateByIndexResponse(request);
        Assert.assertEquals(TEST_STRING3, response.getProject().getName());
        Assert.assertEquals(TEST_STRING3, response.getProject().getDescription());
    }

}
