package ru.t1.azarin.tm.api.repository;

import ru.t1.azarin.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}
