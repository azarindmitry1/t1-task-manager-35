package ru.t1.azarin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.azarin.tm.api.repository.ISessionRepository;
import ru.t1.azarin.tm.marker.UnitCategory;
import ru.t1.azarin.tm.model.Session;

import static ru.t1.azarin.tm.constant.SessionTestData.USER1_SESSION1;
import static ru.t1.azarin.tm.constant.SessionTestData.USER1_SESSION_LIST;
import static ru.t1.azarin.tm.constant.UserTestData.USER1;

@Category(UnitCategory.class)
public final class SessionRepositoryTest {

    @NotNull
    private final ISessionRepository repository = new SessionRepository();

    @Before
    public void before() {
        repository.add(USER1_SESSION1);
    }

    @After
    public void after() {
        repository.remove(USER1_SESSION1);
    }

    @Test
    public void add() {
        Assert.assertNotNull(repository.add(USER1.getId(), USER1_SESSION1));
        @Nullable final Session session = repository.findOneById(USER1.getId(), USER1_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER1_SESSION1, session);
    }

    @Test
    public void clear() {
        Assert.assertFalse(repository.findAll().isEmpty());
        repository.clear(USER1.getId());
        Assert.assertEquals(0, repository.findAll(USER1.getId()).size());
    }

    @Test
    public void findAll() {
        Assert.assertEquals(USER1_SESSION_LIST, repository.findAll());
    }

    @Test
    public void findOneById() {
        @Nullable final Session session = repository.findOneById(USER1.getId(), USER1_SESSION1.getId());
        Assert.assertEquals(USER1_SESSION1, session);
    }

    @Test
    public void findOneByIndex() {
        @Nullable final Session session = repository.findOneByIndex(USER1.getId(), repository.findAll().indexOf(USER1_SESSION1));
        Assert.assertEquals(USER1_SESSION1, session);
    }

    @Test
    public void existById() {
        Assert.assertTrue(repository.existById(USER1.getId(), USER1_SESSION1.getId()));
    }

    @Test
    public void remove() {
        @Nullable final Session session = repository.findOneById(USER1.getId(), USER1_SESSION1.getId());
        repository.remove(USER1.getId(), session);
        Assert.assertNull(repository.findOneById(USER1.getId(), USER1_SESSION1.getId()));
    }

    @Test
    public void removeById() {
        @Nullable final Session session = repository.findOneById(USER1.getId(), USER1_SESSION1.getId());
        repository.removeById(USER1.getId(), session.getId());
        Assert.assertNull(repository.findOneById(USER1.getId(), USER1_SESSION1.getId()));
    }

    @Test
    public void removeByIndex() {
        final Integer index = repository.findAll().indexOf(USER1_SESSION1);
        repository.removeByIndex(USER1.getId(), index);
        Assert.assertNull(repository.findOneById(USER1.getId(), USER1_SESSION1.getId()));
    }

}
