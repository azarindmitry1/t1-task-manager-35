package ru.t1.azarin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.azarin.tm.api.repository.IProjectRepository;
import ru.t1.azarin.tm.api.repository.ITaskRepository;
import ru.t1.azarin.tm.api.repository.IUserRepository;
import ru.t1.azarin.tm.api.service.IPropertyService;
import ru.t1.azarin.tm.api.service.IUserService;
import ru.t1.azarin.tm.exception.entity.UserNotFoundException;
import ru.t1.azarin.tm.exception.field.EmailEmptyException;
import ru.t1.azarin.tm.exception.field.IdEmptyException;
import ru.t1.azarin.tm.exception.field.LoginEmptyException;
import ru.t1.azarin.tm.exception.field.PasswordEmptyException;
import ru.t1.azarin.tm.exception.user.ExistEmailException;
import ru.t1.azarin.tm.exception.user.ExistLoginException;
import ru.t1.azarin.tm.marker.UnitCategory;
import ru.t1.azarin.tm.model.User;
import ru.t1.azarin.tm.repository.ProjectRepository;
import ru.t1.azarin.tm.repository.TaskRepository;
import ru.t1.azarin.tm.repository.UserRepository;

import static ru.t1.azarin.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserServiceTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserService service = new UserService(
            propertyService, userRepository, projectRepository, taskRepository
    );

    @NotNull
    private final String EMPTY_STRING = "";

    @Nullable
    private final String NULL_STRING = null;

    @NotNull
    private final String TEST_STRING = "TEST_STRING";

    @Nullable
    private final Integer NULL_INTEGER = null;

    @Before
    public void before() {
        userRepository.add(USER1);
        userRepository.add(ADMIN);
    }

    @After
    public void after() {
        userRepository.remove(USER1);
        userRepository.remove(ADMIN);
    }

    @Test
    public void create() {
        Assert.assertThrows(LoginEmptyException.class, () ->
                service.create(EMPTY_STRING, USER_UNREGISTRY_PASSWORD, USER_UNREGISTRY_EMAIL)
        );
        Assert.assertThrows(LoginEmptyException.class, () ->
                service.create(NULL_STRING, USER_UNREGISTRY_PASSWORD, USER_UNREGISTRY_EMAIL)
        );
        Assert.assertThrows(ExistLoginException.class, () ->
                service.create(USER1.getLogin(), USER1.getPasswordHash(), USER1.getEmail())
        );
        Assert.assertThrows(PasswordEmptyException.class, () ->
                service.create(USER_UNREGISTRY_LOGIN, EMPTY_STRING, USER_UNREGISTRY_EMAIL)
        );
        Assert.assertThrows(PasswordEmptyException.class, () ->
                service.create(USER_UNREGISTRY_LOGIN, NULL_STRING, USER_UNREGISTRY_EMAIL)
        );
        Assert.assertThrows(ExistEmailException.class, () ->
                service.create(USER_UNREGISTRY_LOGIN, USER_UNREGISTRY_PASSWORD, USER1.getEmail())
        );
        @Nullable final User user = service.create(
                USER_UNREGISTRY_LOGIN, USER_UNREGISTRY_PASSWORD, USER_UNREGISTRY_EMAIL
        );
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_UNREGISTRY_LOGIN, user.getLogin());
        Assert.assertEquals(USER_UNREGISTRY_EMAIL, user.getEmail());
        Assert.assertEquals(service.findOneById(user.getId()), user);
    }

    @Test
    public void findByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () ->
                service.findByLogin(EMPTY_STRING)
        );
        Assert.assertThrows(LoginEmptyException.class, () ->
                service.findByLogin(NULL_STRING)
        );
        @Nullable final User user = service.findByLogin(USER1.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER1, user);
    }

    @Test
    public void findByEmail() {
        Assert.assertThrows(EmailEmptyException.class, () ->
                service.findByEmail(EMPTY_STRING)
        );
        Assert.assertThrows(EmailEmptyException.class, () ->
                service.findByEmail(NULL_STRING)
        );
        @Nullable final User user = service.findByEmail(USER1.getEmail());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER1, user);
    }

    @Test
    public void isLoginExist() {
        Assert.assertFalse(service.isLoginExist(EMPTY_STRING));
        Assert.assertFalse(service.isLoginExist(NULL_STRING));
        Assert.assertTrue(service.isLoginExist(USER1.getLogin()));
        Assert.assertFalse(service.isLoginExist(USER_UNREGISTRY_LOGIN));
    }

    @Test
    public void isEmailExist() {
        Assert.assertFalse(service.isEmailExist(EMPTY_STRING));
        Assert.assertFalse(service.isEmailExist(NULL_STRING));
        Assert.assertTrue(service.isEmailExist(USER1.getEmail()));
        Assert.assertFalse(service.isEmailExist(USER_UNREGISTRY_EMAIL));
    }

    @Test
    public void remove() {
        Assert.assertNull(service.remove(null));
        @Nullable final User user = service.findByLogin(USER1.getLogin());
        service.remove(user);
        Assert.assertNull(service.findOneById(USER1.getId()));
    }

    @Test
    public void removeByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () ->
                service.removeByLogin(EMPTY_STRING)
        );
        Assert.assertThrows(LoginEmptyException.class, () ->
                service.removeByLogin(NULL_STRING)
        );
        Assert.assertThrows(UserNotFoundException.class, () ->
                service.removeByLogin(USER_UNREGISTRY_LOGIN)
        );
        service.removeByLogin(USER1.getLogin());
        Assert.assertNull(service.findOneById(USER1.getId()));
    }

    @Test
    public void removeByEmail() {
        Assert.assertThrows(EmailEmptyException.class, () ->
                service.removeByEmail(EMPTY_STRING)
        );
        Assert.assertThrows(EmailEmptyException.class, () ->
                service.removeByEmail(NULL_STRING)
        );
        Assert.assertThrows(UserNotFoundException.class, () ->
                service.removeByEmail(USER_UNREGISTRY_EMAIL)
        );
        service.removeByEmail(USER1.getEmail());
        Assert.assertNull(service.findOneById(USER1.getId()));
    }

    @Test
    public void setPassword() {
        Assert.assertThrows(IdEmptyException.class, () ->
                service.setPassword(EMPTY_STRING, USER_UNREGISTRY_PASSWORD)
        );
        Assert.assertThrows(IdEmptyException.class, () ->
                service.setPassword(NULL_STRING, USER_UNREGISTRY_PASSWORD)
        );
        Assert.assertThrows(PasswordEmptyException.class, () ->
                service.setPassword(USER_UNREGISTRY_ID, EMPTY_STRING)
        );
        Assert.assertThrows(PasswordEmptyException.class, () ->
                service.setPassword(USER_UNREGISTRY_ID, NULL_STRING)
        );
        Assert.assertThrows(UserNotFoundException.class, () ->
                service.setPassword(USER_UNREGISTRY_ID, USER_UNREGISTRY_PASSWORD)
        );
        @Nullable final User user = service.setPassword(USER1.getId(), TEST_STRING);
        Assert.assertEquals(USER1.getPasswordHash(), user.getPasswordHash());
    }

    @Test
    public void updateUser() {
        Assert.assertThrows(IdEmptyException.class, () ->
                service.updateUser(EMPTY_STRING, TEST_STRING, TEST_STRING, TEST_STRING)
        );
        Assert.assertThrows(IdEmptyException.class, () ->
                service.updateUser(NULL_STRING, TEST_STRING, TEST_STRING, TEST_STRING)
        );
        Assert.assertThrows(UserNotFoundException.class, () ->
                service.updateUser(USER_UNREGISTRY_ID, TEST_STRING, TEST_STRING, TEST_STRING)
        );
        @Nullable final User user = service.updateUser(USER1.getId(), TEST_STRING, TEST_STRING, TEST_STRING);
        Assert.assertEquals(USER1.getFirstName(), user.getFirstName());
        Assert.assertEquals(USER1.getMiddleName(), user.getMiddleName());
        Assert.assertEquals(USER1.getLastName(), user.getLastName());
    }

    @Test
    public void lockUserByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () ->
                service.lockUserByLogin(EMPTY_STRING)
        );
        Assert.assertThrows(LoginEmptyException.class, () ->
                service.lockUserByLogin(NULL_STRING)
        );
        Assert.assertThrows(UserNotFoundException.class, () ->
                service.lockUserByLogin(USER_UNREGISTRY_ID)
        );
        service.lockUserByLogin(USER1.getLogin());
        Assert.assertTrue(USER1.isLocked());
    }

    @Test
    public void unlockUserByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () ->
                service.unlockUserByLogin(EMPTY_STRING)
        );
        Assert.assertThrows(LoginEmptyException.class, () ->
                service.unlockUserByLogin(NULL_STRING)
        );
        Assert.assertThrows(UserNotFoundException.class, () ->
                service.unlockUserByLogin(USER_UNREGISTRY_ID)
        );
        service.lockUserByLogin(USER1.getLogin());
        Assert.assertTrue(USER1.isLocked());
        service.unlockUserByLogin(USER1.getLogin());
        Assert.assertFalse(USER1.isLocked());
    }

}
