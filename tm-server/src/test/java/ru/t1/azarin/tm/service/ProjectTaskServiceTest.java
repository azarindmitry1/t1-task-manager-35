package ru.t1.azarin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.azarin.tm.api.repository.IProjectRepository;
import ru.t1.azarin.tm.api.repository.ITaskRepository;
import ru.t1.azarin.tm.api.service.IProjectTaskService;
import ru.t1.azarin.tm.exception.field.ProjectIdEmptyException;
import ru.t1.azarin.tm.exception.field.TaskIdEmptyException;
import ru.t1.azarin.tm.exception.field.UserIdEmptyException;
import ru.t1.azarin.tm.marker.UnitCategory;
import ru.t1.azarin.tm.model.Task;
import ru.t1.azarin.tm.repository.ProjectRepository;
import ru.t1.azarin.tm.repository.TaskRepository;

import static ru.t1.azarin.tm.constant.ProjectTestData.USER1_PROJECT1;
import static ru.t1.azarin.tm.constant.ProjectTestData.USER1_PROJECT2;
import static ru.t1.azarin.tm.constant.TaskTestData.USER1_TASK1;
import static ru.t1.azarin.tm.constant.TaskTestData.USER1_TASK2;
import static ru.t1.azarin.tm.constant.UserTestData.USER1;

@Category(UnitCategory.class)
public final class ProjectTaskServiceTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectTaskService service = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final String EMPTY_STRING = "";

    @Nullable
    private final String NULL_STRING = null;

    @NotNull
    private final String TEST_STRING = "TEST_STRING";

    @Nullable
    private final Integer NULL_INTEGER = null;

    @Before
    public void before() {
        projectRepository.add(USER1_PROJECT1);
        projectRepository.add(USER1_PROJECT2);
        taskRepository.add(USER1_TASK1);
        taskRepository.add(USER1_TASK2);
    }

    @After
    public void after() {
        projectRepository.remove(USER1_PROJECT1);
        projectRepository.remove(USER1_PROJECT2);
        taskRepository.remove(USER1_TASK1);
        taskRepository.remove(USER1_TASK2);
    }

    @Test
    public void bindTaskToProject() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.bindTaskToProject(EMPTY_STRING, USER1_PROJECT1.getId(), USER1_TASK1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.bindTaskToProject(NULL_STRING, USER1_PROJECT1.getId(), USER1_TASK1.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.bindTaskToProject(USER1.getId(), EMPTY_STRING, USER1_TASK1.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.bindTaskToProject(USER1.getId(), NULL_STRING, USER1_TASK1.getId()));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.bindTaskToProject(USER1.getId(), USER1_PROJECT1.getId(), EMPTY_STRING));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.bindTaskToProject(USER1.getId(), USER1_PROJECT1.getId(), NULL_STRING));
        @Nullable final Task task = taskRepository.create(USER1.getId(), TEST_STRING, TEST_STRING);
        service.bindTaskToProject(USER1.getId(), USER1_PROJECT1.getId(), task.getId());
        Assert.assertEquals(task.getProjectId(), USER1_PROJECT1.getId());
    }

    @Test
    public void unbindTaskToProject() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.unbindTaskToProject(EMPTY_STRING, USER1_PROJECT1.getId(), USER1_TASK1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.unbindTaskToProject(NULL_STRING, USER1_PROJECT1.getId(), USER1_TASK1.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.unbindTaskToProject(USER1.getId(), EMPTY_STRING, USER1_TASK1.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.unbindTaskToProject(USER1.getId(), NULL_STRING, USER1_TASK1.getId()));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.unbindTaskToProject(USER1.getId(), USER1_PROJECT1.getId(), EMPTY_STRING));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.unbindTaskToProject(USER1.getId(), USER1_PROJECT1.getId(), NULL_STRING));
        service.unbindTaskToProject(USER1.getId(), USER1_PROJECT1.getId(), USER1_TASK1.getId());
        Assert.assertNull(USER1_TASK1.getProjectId());
    }

    @Test
    public void removeProjectById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeProjectById(EMPTY_STRING, USER1_PROJECT1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeProjectById(NULL_STRING, USER1_PROJECT1.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.removeProjectById(USER1.getId(), EMPTY_STRING));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.removeProjectById(USER1.getId(), NULL_STRING));
        service.removeProjectById(USER1.getId(), USER1_PROJECT1.getId());
        Assert.assertNull(projectRepository.findOneById(USER1_PROJECT1.getId()));
    }

}
